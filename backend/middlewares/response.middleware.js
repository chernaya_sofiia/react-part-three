const responseMiddleware = (req, res, next) => {
  if (res.err) {
    const { status, message } = res.err;
    res.status(status).json({
      error: true,
      message
    });
  }
  res.status(200).json(res.data);
  next();
};

exports.responseMiddleware = responseMiddleware;
