const UserService = require('../services/userService');

const getUsers = (req, res, next) => {
    try {
        const users = UserService.getUsers();
        res.data = users;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}

const getUser = (req, res, next) => {
    try {
        const id = req.params.id;
        const user = UserService.getUser(id);
        res.data = user;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}

const createUser = (req, res, next) => {
    if (res.err) {
        return next();
    }
    try {
        const data = req.body;
        const createdUser = UserService.createUser(data);
        res.data = createdUser;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}

const updateUser = (req, res, next) => {
    if (res.err) {
        return next();
    }
    try {
        const id = req.params.id;
        const data = req.body;
        const updatedUser = UserService.updateUser(id, data);
        res.data = updatedUser;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}

const deleteUser = (req, res, next) => {
    try {
        const id = req.params.id;
        const deletedUser = UserService.deleteUser(id);
        res.data = deletedUser;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}

exports.getUsers = getUsers;
exports.getUser = getUser;
exports.updateUser = updateUser;
exports.createUser = createUser;
exports.deleteUser = deleteUser;
