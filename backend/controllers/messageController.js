const MessageService = require('../services/messageService');

const getMessages = (req, res, next) => {
    try {
        const messages = MessageService.getMessages();
        res.data = messages;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}

const getMessage = (req, res, next) => {
    try {
        const id = req.params.id;
        const message = MessageService.getMessage(id);
        res.data = message;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}

const createMessage = (req, res, next) => {
    if (res.err) {
        return next();
    }
    try {
        const data = req.body;
        const createdMessage = MessageService.createMessage(data);
        res.data = createdMessage;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}

const updateMessage = (req, res, next) => {
    if (res.err) {
        return next();
    }
    try {
        const id = req.params.id;
        const data = req.body;
        const updatedMessage = MessageService.updateMessage(id, data);
        res.data = updatedMessage;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}

const deleteMessage = (req, res, next) => {
    try {
        const id = req.params.id;
        const deletedMessage = MessageService.deleteMessage(id);
        res.data = deletedMessage;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}

exports.getMessages = getMessages;
exports.getMessage = getMessage;
exports.updateMessage = updateMessage;
exports.createMessage = createMessage;
exports.deleteMessage = deleteMessage;
