const UserService = require('./userService');
const { BadRequest, NotFound } = require('../helpers/customErrors');

class AuthService {
  login(userData) {
    const { login, password } = userData;
    const user = UserService.search({ login });
    if (!user) {
      throw new NotFound('User not found');
    } else if (user.password !== password) {
      throw new BadRequest('Wrong password');
    }
    return user;
  }
}

module.exports = new AuthService();
