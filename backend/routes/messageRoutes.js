const { Router } = require('express');
const { getMessages, getMessage, createMessage, updateMessage, deleteMessage } = require('../controllers/messageController');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createMessageValid, updateMessageValid } = require('../middlewares/message.validation.middleware');

const router = Router();

router.get('/', getMessages, responseMiddleware);

router.get('/:id', getMessage, responseMiddleware);

router.post('/', createMessageValid, createMessage, responseMiddleware);

router.put('/:id', updateMessageValid, updateMessage, responseMiddleware);

router.delete('/:id', deleteMessage, responseMiddleware);

module.exports = router;
