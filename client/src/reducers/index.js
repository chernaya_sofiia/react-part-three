import { combineReducers } from 'redux';
import profile from '../containers/LoginPage/reducer';
import messages from '../containers/ChatPage/reducer';
import message from '../containers/MessagePage/reducer';
import users from '../containers/UsersPage/reducer';
import user from '../containers/UserPage/reducer';

const rootReducer = combineReducers({
  profile,
  messages,
  message,
  users,
  user
});

export default rootReducer;
