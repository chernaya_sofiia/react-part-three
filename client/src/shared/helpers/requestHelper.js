const apiUrl = '/api';

export const getReq = async (entityName, id = '') => {
  return await makeRequest(`${entityName}/${id}`, 'GET');
};

export const postReq = async (entityName, body) => {
  return await makeRequest(entityName, 'POST', body);
};

export const putReq = async (entityName, id, body) => {
  return await makeRequest(`${entityName}/${id}`, 'PUT', body);
};

export const deleteReq = async (entityName, id) => {
  return await makeRequest(`${entityName}/${id}`, 'DELETE');
};

const makeRequest = async (path, method, body) => {
  const url = `${apiUrl}/${path}`;
  const res = await fetch(url, {
    method,
    body: body ? JSON.stringify(body) : undefined,
    headers: { 'Content-Type': 'application/json' }
  });

  const dataObj = await res.json();
  if (!res.ok) {
    throw new Error(dataObj.message);
  }
  return dataObj;
};
