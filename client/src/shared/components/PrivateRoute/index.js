import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

const PrivateRoute = ({ profile, component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={props =>
        profile.data && profile.data.id ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: '/login',
              state: { from: props.location }
            }}
          />
        )
      }
    />
  );
};
const mapStateToProps = state => ({
  profile: state.profile
});

export default connect(mapStateToProps)(PrivateRoute);
