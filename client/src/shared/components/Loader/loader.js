import React from 'react';

import './loader.css';

const Loader = () => {
  return (
    <div className="loadingio-spinner-rolling-r6ivjje215">
      <div className="ldio-gucocuficju">
        <div />
      </div>
    </div>
  );
};

export default Loader;
