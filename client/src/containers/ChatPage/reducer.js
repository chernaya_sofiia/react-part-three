import { FETCH_MESSAGES, FETCH_MESSAGES_ERROR, FETCH_MESSAGES_SUCCESS } from './actionTypes';

const initialState = {
  isLoading: true,
  error: false,
  data: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case FETCH_MESSAGES: {
      return { ...state, isLoading: true };
    }
    case FETCH_MESSAGES_ERROR: {
      const { error } = action.payload;
      return {
        isLoading: false,
        error,
        data: []
      };
    }
    case FETCH_MESSAGES_SUCCESS: {
      const { data } = action.payload;
      return {
        isLoading: false,
        error: false,
        data
      };
    }

    default:
      return state;
  }
}
