export const createMessages = data => {
  if (!data) {
    return [];
  }
  const sortedMessages = data
    .map(message => {
      const createdAt = formatDate(message.createdAt);
      return { ...message, createdAt };
    })
    .sort((a, b) => a.createdAt - b.createdAt);
  const messages = sortedMessages.map(message => {
    const { id, text, userId, user, avatar, createdAt, editedAt, likes } = message;
    const [date, time] = getDate(createdAt);
    const isEdited = editedAt !== '';
    return { id, text, userId, user, avatar, date, time, likes, isEdited };
  });
  return messages;
};

export const getUsersCount = messages => {
  return messages.length && new Set(messages.map(message => message.user)).size;
};

export const getMessagesCount = messages => messages.length;

export const getLastMessageTime = messages => (!messages.length ? 'no messages' : messages.slice(-1)[0].time);

export const getUserLastMessage = (messages, userId) => {
  const allUserMessages = messages.filter(message => message.userId === userId);
  if (allUserMessages.length) {
    const { id, text } = allUserMessages.slice(-1)[0];
    return { isMessageExist: true, id, text };
  }
  return { isMessageExist: false, id: '', text: '' };
};

export const checkIsLiked = (likes, userId) => {
  return likes.find(id => id === userId);
};

export const toggleMessageReact = (likes, userId) => {
  const isLike = checkIsLiked(likes, userId);
  if (isLike) {
    return likes.filter(id => id !== userId);
  }
  likes.push(userId);
  console.log(likes);
  return likes;
};

const getDate = gettingDate => {
  const formatedDate = gettingDate.toString();
  const date = formatedDate.substring(0, 10);
  const time = formatedDate.substring(15, 21);
  return [date, time];
};

const formatDate = isoDate => {
  return new Date(isoDate);
};
