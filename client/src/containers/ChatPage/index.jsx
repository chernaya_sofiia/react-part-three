import React, { Component } from 'react';

import { connect } from 'react-redux';
import { fetchMessages, deleteMessage, addMessage, updateMessage } from './actions';
import { logOut } from '../LoginPage/actions';
import { getUsersCount, getMessagesCount, getLastMessageTime, getUserLastMessage, toggleMessageReact } from './helper';

import ChatHeader from './components/ChatHeader';
import MessageList from './components/MessageList';
import MessageInput from './components/MessageInput';

import Loader from '../../shared/components/Loader';
import Header from '../../shared/components/Header';

import './index.css';

class Chat extends Component {
  constructor(props) {
    super(props);
    this.onMessageEdit = this.onMessageEdit.bind(this);
    this.onEditLastMessage = this.onEditLastMessage.bind(this);
    this.onMessageLike = this.onMessageLike.bind(this);
  }
  componentDidMount() {
    this.props.fetchMessages();
    document.addEventListener('keydown', this.onEditLastMessage);
  }
  componentWillUnmount() {
    document.removeEventListener('keydown', this.onEditLastMessage);
  }

  onMessageEdit(id) {
    this.props.history.push(`/message/${id}`);
  }

  onMessageLike(id, likes, userId) {
    const updatedLikes = toggleMessageReact(likes, userId);
    this.props.updateMessage(id, { likes: updatedLikes });
  }

  onEditLastMessage(event) {
    if (event.keyCode === 38) {
      const { messages, profile } = this.props;
      const { isMessageExist, id } = getUserLastMessage(messages.data, profile.id);
      if (isMessageExist) {
        this.onMessageEdit(id);
      }
      return;
    }
  }

  render() {
    const { profile, messages, deleteMessage, addMessage, logOut } = this.props;
    const usersCount = getUsersCount(messages.data);
    const messagesCount = getMessagesCount(messages.data);
    const lastMessageTime = getLastMessageTime(messages.data);
    const chatName = 'Saga chat';

    return (
      <>
        {messages.isLoading ? (
          <Loader />
        ) : (
          <>
            <Header logOut={logOut} profile={profile} />
            <div className="chat">
              <ChatHeader
                chatName={chatName}
                usersCount={usersCount}
                messagesCount={messagesCount}
                lastMessageTime={lastMessageTime}
              />
              <MessageList
                currentUserId={profile.id}
                messages={messages.data}
                onMessageDelete={deleteMessage}
                onMessageEdit={this.onMessageEdit}
                onMessageLike={this.onMessageLike}
              />
              <MessageInput onMessageAdd={addMessage} user={profile.name} avatar={profile.avatar} userId={profile.id} />
            </div>
          </>
        )}
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    profile: state.profile.data,
    messages: state.messages
  };
};

const mapDispatchToProps = {
  fetchMessages,
  deleteMessage,
  addMessage,
  updateMessage,
  logOut
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
