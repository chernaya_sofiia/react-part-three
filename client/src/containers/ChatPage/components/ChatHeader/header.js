import React from 'react';
import PropTypes from 'prop-types';

import './chatHeader.css';

export const ChatHeader = ({ chatName, usersCount, messagesCount, lastMessageTime }) => {
  return (
    <header className="chat-header">
      <div className="chat-header__left">
        <h3 className="chat-header__name">{chatName}</h3>
        <div className="chat-header__users">
          <p>
            <i className="fa fa-user icon" />
            Users
          </p>
          <p>{usersCount}</p>
        </div>
        <div className="chat-header__messages">
          <p>
            <i className="fa fa-comments icon" />
            Messages
          </p>
          <p>{messagesCount}</p>
        </div>
      </div>
      <div className="chat-header__time">
        <p>
          <i className="fa fa-clock-o icon" />
          Last message
        </p>
        <p>{lastMessageTime}</p>
      </div>
    </header>
  );
};

ChatHeader.propTypes = {
  headerInfo: PropTypes.shape({
    chatName: PropTypes.string.isRequired,
    usersCount: PropTypes.number.isRequired,
    messagesCount: PropTypes.number.isRequired,
    lastMessageTime: PropTypes.number.isRequired
  })
};
