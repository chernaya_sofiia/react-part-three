import { FETCH_MESSAGES, DELETE_MESSAGE, ADD_MESSAGE, UPDATE_MESSAGE } from './actionTypes';

export const fetchMessages = () => ({
  type: FETCH_MESSAGES
});

export const deleteMessage = id => ({
  type: DELETE_MESSAGE,
  payload: {
    id
  }
});

export const addMessage = newMessage => ({
  type: ADD_MESSAGE,
  payload: {
    newMessage
  }
});

export const updateMessage = (id, data) => ({
  type: UPDATE_MESSAGE,
  payload: {
    id,
    data
  }
});
