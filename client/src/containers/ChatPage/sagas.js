import { call, put, takeEvery, all } from 'redux-saga/effects';
import {
  FETCH_MESSAGES,
  FETCH_MESSAGES_ERROR,
  FETCH_MESSAGES_SUCCESS,
  DELETE_MESSAGE,
  ADD_MESSAGE,
  UPDATE_MESSAGE
} from './actionTypes';
import { getMessages, removeMessage, createMessage, changeMessage } from './service';

export function* fetchMessages() {
  try {
    const messages = yield call(getMessages);
    yield put({ type: FETCH_MESSAGES_SUCCESS, payload: { data: messages } });
  } catch (error) {
    yield put({ type: FETCH_MESSAGES_ERROR, payload: { error: error.message } });
  }
}

function* watchFetchMessages() {
  yield takeEvery(FETCH_MESSAGES, fetchMessages);
}

export function* deleteMessage(action) {
  const { id } = action.payload;
  yield call(removeMessage, id);
  yield put({ type: FETCH_MESSAGES });
}

function* watchDeleteMessage() {
  yield takeEvery(DELETE_MESSAGE, deleteMessage);
}

export function* addMessage(action) {
  const { newMessage } = action.payload;
  yield call(createMessage, newMessage);
  yield put({ type: FETCH_MESSAGES });
}

function* watchAddMessage() {
  yield takeEvery(ADD_MESSAGE, addMessage);
}

export function* updateMessage(action) {
  const { id, data } = action.payload;
  yield call(changeMessage, id, data);
  yield put({ type: FETCH_MESSAGES });
}

function* watchUpdateMessage() {
  yield takeEvery(UPDATE_MESSAGE, updateMessage);
}

export default function* messagesSagas() {
  yield all([watchFetchMessages(), watchDeleteMessage(), watchAddMessage(), watchUpdateMessage()]);
}
