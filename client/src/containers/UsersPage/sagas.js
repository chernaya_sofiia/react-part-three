import { call, put, takeEvery, all } from 'redux-saga/effects';
import { FETCH_USERS, FETCH_USERS_ERROR, FETCH_USERS_SUCCESS, DELETE_USER } from './actionTypes';
import { getUsers, removeUser } from './service';

export function* fetchUsers() {
  try {
    const users = yield call(getUsers);
    yield put({ type: FETCH_USERS_SUCCESS, payload: { data: users } });
  } catch (error) {
    yield put({ type: FETCH_USERS_ERROR, payload: { error: error.message } });
  }
}

function* watchFetchUsers() {
  yield takeEvery(FETCH_USERS, fetchUsers);
}

export function* deleteUser(action) {
  try {
    const { id } = action.payload;
    yield call(removeUser, id);
    yield put({ type: FETCH_USERS });
  } catch (error) {
    yield put({ type: FETCH_USERS_ERROR, payload: { error: error.message } });
  }
}

function* watchDeleteUser() {
  yield takeEvery(DELETE_USER, deleteUser);
}

export default function* usersSagas() {
  yield all([watchFetchUsers(), watchDeleteUser()]);
}
