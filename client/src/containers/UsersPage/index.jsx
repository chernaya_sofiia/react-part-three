import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { fetchUsers, deleteUser } from './actions';
import { logOut } from '../LoginPage/actions';

import UsersHeader from './components/UsersHeader';
import UsersList from './components/UsersList';

import Header from '../../shared/components/Header';
import Loader from '../../shared/components/Loader';
import Error from '../../shared/components/Error';

import './index.css';

const UsersPage = props => {
  const { users, fetchUsers, deleteUser, profile, logOut } = props;

  useEffect(() => {
    fetchUsers();
  }, [fetchUsers]);

  const onUserUpdate = id => {
    props.history.push(`/user/${id}`);
  };

  const onUserCreate = () => {
    props.history.push('/user');
  };

  return (
    <>
      <Header logOut={logOut} profile={profile} />
      <div className="users-page">
        {users.error && <Error message={users.error} />}
        {users.isLoading ? (
          <Loader />
        ) : (
          <>
            <UsersHeader />
            <UsersList users={users.data} onDeleteUser={deleteUser} onUserUpdate={onUserUpdate} />
            <button className="users-page__button" onClick={onUserCreate}>
              Create new user
            </button>
          </>
        )}
      </div>
    </>
  );
};

const mapStateToProps = state => ({
  users: state.users,
  profile: state.profile.data
});

const mapDispatchToProps = {
  fetchUsers,
  deleteUser,
  logOut
};

export default connect(mapStateToProps, mapDispatchToProps)(UsersPage);
