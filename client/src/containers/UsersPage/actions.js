import { FETCH_USERS, DELETE_USER } from './actionTypes';

export const fetchUsers = () => ({
  type: FETCH_USERS
});

export const deleteUser = id => ({
  type: DELETE_USER,
  payload: {
    id
  }
});
