import { LOGIN_USER, LOG_OUT } from './actionTypes';

export const loginUser = userData => {
  return {
    type: LOGIN_USER,
    payload: {
      userData
    }
  };
};

export const logOut = () => {
  return {
    type: LOG_OUT
  };
};
