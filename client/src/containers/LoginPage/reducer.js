import { LOGIN_USER, LOGIN_USER_ERROR, LOGIN_USER_SUCCESS, LOG_OUT } from './actionTypes';

const initialState = {
  isLoading: false,
  error: false,
  data: null
};

export default function(state = initialState, action) {
  switch (action.type) {
    case LOGIN_USER: {
      return { ...state, error: false, isLoading: true };
    }
    case LOGIN_USER_ERROR: {
      const { error } = action.payload;
      return {
        ...state,
        isLoading: false,
        error
      };
    }
    case LOGIN_USER_SUCCESS: {
      const { data } = action.payload;
      return {
        ...state,
        error: false,
        isLoading: false,
        data
      };
    }
    case LOG_OUT: {
      return {
        ...state,
        isLoading: false,
        error: false,
        data: null
      };
    }

    default:
      return state;
  }
}
