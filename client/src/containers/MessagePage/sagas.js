import { call, put, takeEvery, all } from 'redux-saga/effects';
import { FETCH_MESSAGE, FETCH_MESSAGE_ERROR, FETCH_MESSAGE_SUCCESS } from './actionTypes';
import { getMessage } from './service';

export function* fetchMessage(action) {
  const { id } = action.payload;
  try {
    const message = yield call(getMessage, id);
    yield put({ type: FETCH_MESSAGE_SUCCESS, payload: { data: message } });
  } catch (error) {
    yield put({ type: FETCH_MESSAGE_ERROR, payload: { error: error.message } });
  }
}

function* watchFetchMessage() {
  yield takeEvery(FETCH_MESSAGE, fetchMessage);
}

export default function* messageSagas() {
  yield all([watchFetchMessage()]);
}
