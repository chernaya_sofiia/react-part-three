import { FETCH_MESSAGE, FETCH_MESSAGE_ERROR, FETCH_MESSAGE_SUCCESS } from './actionTypes';

const initialState = {
  isLoading: true,
  error: false,
  data: {}
};

export default function(state = initialState, action) {
  switch (action.type) {
    case FETCH_MESSAGE: {
      return { ...state, error: false, isLoading: true };
    }
    case FETCH_MESSAGE_ERROR: {
      const { error } = action.payload;
      return {
        ...state,
        isLoading: false,
        error
      };
    }
    case FETCH_MESSAGE_SUCCESS: {
      const { data } = action.payload;
      return {
        ...state,
        isLoading: false,
        error: false,
        data
      };
    }

    default:
      return state;
  }
}
