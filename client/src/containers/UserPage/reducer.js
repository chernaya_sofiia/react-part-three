import {
  FETCH_USER,
  FETCH_USER_ERROR,
  FETCH_USER_SUCCESS,
  ADD_USER,
  ADD_USER_ERROR,
  ADD_USER_SUCCESS,
  UPDATE_USER,
  UPDATE_USER_ERROR,
  UPDATE_USER_SUCCESS,
  LEAVE_USER_PAGE
} from './actionTypes';

const initialState = {
  isLoading: true,
  error: false,
  success: false,
  notFound: false,
  data: null
};

export default function(state = initialState, action) {
  switch (action.type) {
    case FETCH_USER: {
      return { ...state, isLoading: true };
    }
    case FETCH_USER_ERROR: {
      const { error } = action.payload;
      return {
        ...state,
        isLoading: false,
        notFound: error
      };
    }
    case FETCH_USER_SUCCESS: {
      const { data } = action.payload;
      return {
        ...state,
        isLoading: false,
        data
      };
    }
    case ADD_USER: {
      return {
        ...state,
        success: false,
        error: false,
        isLoading: true
      };
    }
    case ADD_USER_ERROR: {
      const { error } = action.payload;
      return {
        ...state,
        isLoading: false,
        error
      };
    }
    case ADD_USER_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        success: 'User was created'
      };
    }
    case UPDATE_USER: {
      return {
        ...state,
        error: false,
        success: false,
        isLoading: true
      };
    }
    case UPDATE_USER_ERROR: {
      const { error } = action.payload;
      return {
        ...state,
        isLoading: false,
        error
      };
    }
    case UPDATE_USER_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        success: 'User was updated'
      };
    }
    case LEAVE_USER_PAGE: {
      return {
        ...state,
        isLoading: false,
        error: false,
        success: false,
        notFound: false
      };
    }

    default:
      return state;
  }
}
