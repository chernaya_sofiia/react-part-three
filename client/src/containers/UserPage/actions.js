import { FETCH_USER, ADD_USER, UPDATE_USER, LEAVE_USER_PAGE } from './actionTypes';

export const fetchUser = id => ({
  type: FETCH_USER,
  payload: {
    id
  }
});

export const addUser = newUser => ({
  type: ADD_USER,
  payload: {
    newUser
  }
});

export const updateUser = (id, data) => ({
  type: UPDATE_USER,
  payload: {
    id,
    data
  }
});

export const leaveUserPage = () => ({
  type: LEAVE_USER_PAGE
});
